package com.lee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootApplication
public class LeetApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeetApplication.class, args);
	}

}
