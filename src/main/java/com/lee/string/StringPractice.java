package com.lee.string;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Predicate;

public class StringPractice {

    public static void main(String[] args) {
     String original =  "hello";
     int value=1;
     Map<Character,Integer> map= new HashMap<>();
     for(int i=0;i<original.length();i++) {
         Character key = original.charAt(i);
         if (map.containsKey(key)) {
             value = map.get(key);
             ++value;
             map.replace(key, value);
         } else {
             value = 1;
             map.put(key, value);
         }
     }
            for (Integer value1 : map.values()) {
            System.out.println(value1);
        }





    }

}
