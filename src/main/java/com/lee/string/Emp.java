package com.lee.string;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
public class Emp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
//    @JsonProperty("round_trip1")
//    @Column(columnDefinition = "boolean default false")
//    private boolean roundTrip1 ;
     @JsonProperty("round_trip")
    private Boolean roundTrip = Boolean.FALSE;

@JsonProperty("delivery_type")
private Delivery deliveryType=Delivery.Standard;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Column(name = "round_trip")
    public Boolean getRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }
    @Basic
    @Column(name = "delivery_type", nullable = false, length = 200)
    public Delivery getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(Delivery deliveryType) {
        this.deliveryType = deliveryType;
    }
}
